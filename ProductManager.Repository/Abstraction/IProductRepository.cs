﻿using System.Collections.Generic;
using ProductManager.Entities;

namespace ProductManager.Repository.Abstraction
{
    public interface IProductRepository
    {
        List<Product> GetProducts();

        Product GetProduct(int productId);

        void CreateProduct(Product product);

        void UpdateProduct(Product product);

        void DeleteProduct(int productId);
    }
}
