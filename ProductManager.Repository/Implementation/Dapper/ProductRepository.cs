﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using ProductManager.Entities;
using ProductManager.Repository.Abstraction;

namespace ProductManager.Repository.Implementation.Dapper
{
    public class ProductRepository : IProductRepository
    {
        private readonly string connectionString = "Data Source=PF-0M0GMX;" + "Initial Catalog=OdeToFoodDb;" + "Integrated Security=SSPI;";

        public List<Product> GetProducts()
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {

                IEnumerable<Product> products = con.Query<Product>("SELECT * FROM Product");

                return products.ToList();
            }

        }

        public Product GetProduct(int productId)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {

                Product product = con.Query<Product>("SELECT * FROM Product WHERE ProductId = @ProductId", new
                {
                    ProductId = productId
                }).FirstOrDefault();

                return product;
            }
        }

        public void CreateProduct(Product product)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {

                con.Query("INSERT INTO Product(Name, Price) VALUES (@Name, @Price)", new
                {
                    Name = product.Name,
                    Price = product.Price
                });
            }
        }

        public void UpdateProduct(Product product)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {

                con.Query("UPDATE Product SET Name = @Name, Price = @Price WHERE ProductId = @ProductId", new
                {
                    Name = product.Name,
                    Price = product.Price,
                    ProductId = product.ProductId
                });
            }
        }

        public void DeleteProduct(int productId)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Query("DELETE Product WHERE ProductID = @ProductID", new
                {
                    ProductId = productId
                });
            }
        }
    }
}
