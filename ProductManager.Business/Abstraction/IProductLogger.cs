﻿using ProductManager.Entities;

namespace ProductManager.Business.Abstraction
{
    public interface IProductLogger
    {
        void LogProduct(Product product);
    }
}
