﻿namespace ProductManager.WebClient.Helper
{
    public static class NotificationHelper
    {
        public static string SuccessMessage { get; private set; }

        public static string ErrorMessage { get; private set; }

        public static void PushSuccessMessage(string message)
        {
            SuccessMessage = message;
        }

        public static string PopSuccessMessage()
        {
            string message = SuccessMessage;

            SuccessMessage = null;

            return message;
        }

        public static bool IsThereAnySuccessMessage()
        {
            return !string.IsNullOrEmpty(SuccessMessage);
        }
    }
}