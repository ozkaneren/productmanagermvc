﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using ProductManager.Entities;
using ProductManager.WebClient.Helper;
using ProductManager.WebClient.Models;

namespace ProductManager.WebClient.Controllers
{
    public class ProductController : Controller
    {
        public const int PageSize = 2;

        private readonly Business.Implementation.ProductManager _manager = new Business.Implementation.ProductManager();

        public ActionResult Index(int currentPageIndex = 1)
        {

            return View(this.GetFoods(currentPageIndex));
        }

        private PagingModel GetFoods(int currentPage)
        {
            int maxRows = 3;

            List<Product> products = _manager.GetProducts();
            PagingModel model = new PagingModel();

            model.Product = (from product in products
                             select product)
                .OrderBy(r => r.ProductId)
                .Skip((currentPage - 1) * maxRows)
                .Take(maxRows).ToList();

            double pageCount = (double)((decimal)products.Count() / Convert.ToDecimal(maxRows));
            model.PageCount = (int)Math.Ceiling(pageCount);

            model.CurrentPageIndex = currentPage;

            return model;

        }

        public PartialViewResult Search(string q)
        {
            List<Product> products = _manager.GetProducts();

            if (q.IsNullOrWhiteSpace())
            {
                var search = products.Where(r => r.Name.Contains(q) ||
                                                 String.IsNullOrEmpty(q)).Take(10);
                return PartialView("ProductSearchResults", search);
            }
            else
            {
                if (q.Substring(0, 1).Equals(q.Substring(0, 1).ToUpper()))
                {
                    string secondVersion = q.Substring(0, 1);
                    secondVersion = secondVersion.ToLower();
                    secondVersion = secondVersion + q.Substring(1);
                    var search = products.Where(r => r.Name.Contains(q) || r.Name.Contains(secondVersion) ||
                                                     String.IsNullOrEmpty(q)).Take(10);

                    return PartialView("ProductSearchResults", search);
                }
                else
                {
                    string secondVersion = q.Substring(0, 1);
                    secondVersion = secondVersion.ToUpper();
                    secondVersion = secondVersion + q.Substring(1);
                    var search = products.Where(r => r.Name.Contains(q) || r.Name.Contains(secondVersion) ||
                                                     String.IsNullOrEmpty(q)).Take(10);

                    return PartialView("ProductSearchResults", search);
                }

            }

        }



        public ActionResult List()
        {
            List<Product> products = _manager.GetProducts();

            return View(products);
        }




        public ActionResult Details(int id)
        {

            return View(_manager.GetProduct(id));
        }


        public ActionResult Create()
        {

            return View();
        }


        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            Product product = new Product();
            try
            {
                string[] a = new string[3];
                collection.CopyTo(a, 0);
                product.Name = a[1];
                product.Price = Convert.ToDecimal(a[2]);
                _manager.AddProduct(product);
                NotificationHelper.PushSuccessMessage("Success");
                return RedirectToAction("List");
            }
            catch (Exception ex)
            {
                return View();
            }
        }


        public ActionResult Edit(int id)
        {
            Product product = _manager.GetProduct(id);
            return View(product);
        }


        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            Product product = new Product();
            try
            {
                string[] a = new string[3];
                collection.CopyTo(a, 0);
                product.ProductId = id;
                product.Name = a[1];
                product.Price = Convert.ToDecimal(a[2]);
                _manager.UpdateProduct(product);
                NotificationHelper.PushSuccessMessage("Success");
                return RedirectToAction("List");
            }
            catch (Exception ex)
            {
                return View();
            }

        }

        public ActionResult Delete(int id)
        {
            Product product = _manager.GetProduct(id);

            return View(product);
        }


        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {

            try
            {
                _manager.DeleteProduct(id);
                NotificationHelper.PushSuccessMessage("Success");
                return RedirectToAction("List");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult About()
        {
            return View();
        }

    }
}
