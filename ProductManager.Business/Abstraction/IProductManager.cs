﻿using System.Collections.Generic;
using ProductManager.Entities;

namespace ProductManager.Business.Abstraction
{
    public interface IProductManager
    {
        List<Product> GetProducts();

        Product GetProduct(int productId);

        void AddProduct(Product product);

        void UpdateProduct(Product product);

        void DeleteProduct(int productId);
    }
}
