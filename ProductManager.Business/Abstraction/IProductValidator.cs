﻿using ProductManager.Entities;

namespace ProductManager.Business.Abstraction
{
    public interface IProductValidator
    {
        bool ValidateProduct(Product product);
    }
}