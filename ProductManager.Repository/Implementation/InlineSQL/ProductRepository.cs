﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using ProductManager.Entities;
using ProductManager.Repository.Abstraction;

namespace ProductManager.Repository.Implementation.InlineSQL
{
    public class ProductRepository : IProductRepository
    {
        private readonly string connectionString = "Data Source=PF-0M0GMX;" + "Initial Catalog=OdeToFoodDb;" + "Integrated Security=SSPI;";

        public List<Product> GetProducts()
        {
            DataTable dt = new DataTable();



            using (SqlConnection con = new SqlConnection(connectionString))
            {

                con.Open();

                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Product", con))
                {


                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(dt);
                    }

                }

                try
                {
                    List<Product> list = new List<Product>();

                    foreach (var row in dt.AsEnumerable())
                    {
                        Product obj = new Product();

                        foreach (var prop in obj.GetType().GetProperties())
                        {
                            try
                            {
                                PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);
                                propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
                            }
                            catch
                            {
                                continue;
                            }
                        }

                        list.Add(obj);
                    }

                    return list;
                }
                catch
                {
                    return null;
                }

            }

        }

        public Product GetProduct(int productId)
        {
            DataTable dt = new DataTable();

            using (SqlConnection con = new SqlConnection(connectionString))
            {

                con.Open();

                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Product WHERE ProductID = @ID", con))
                {

                    cmd.Parameters.Add("@ID", SqlDbType.Int);
                    cmd.Parameters["@ID"].Value = productId;

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(dt);
                    }

                }

                try
                {
                    Product obj = new Product();

                    foreach (var row in dt.AsEnumerable())
                    {


                        foreach (var prop in obj.GetType().GetProperties())
                        {
                            try
                            {
                                PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);
                                propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
                            }
                            catch
                            {
                                continue;
                            }
                        }


                    }

                    return obj;
                }
                catch
                {
                    return null;
                }

            }
        }

        public void CreateProduct(Product product)
        {

            DataTable dt = new DataTable();

            using (SqlConnection con = new SqlConnection(connectionString))
            {

                con.Open();

                using (SqlCommand cmd = new SqlCommand("INSERT INTO Product(Name, Price) VALUES (@Name, @Price)", con))
                {

                    cmd.Parameters.Add("@Name", SqlDbType.NVarChar);
                    cmd.Parameters.Add("@Price", SqlDbType.Decimal);

                    cmd.Parameters["@Name"].Value = product.Name;
                    cmd.Parameters["@Price"].Value = product.Price;

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(dt);
                    }

                }

            }
        }

        public void UpdateProduct(Product product)
        {
            DataTable dt = new DataTable();

            using (SqlConnection con = new SqlConnection(connectionString))
            {

                con.Open();

                using (SqlCommand cmd = new SqlCommand("UPDATE Product SET Name = @Name, Price = @Price WHERE ProductId = @ProductId", con))
                {

                    cmd.Parameters.Add("@Name", SqlDbType.NVarChar);
                    cmd.Parameters.Add("@Price", SqlDbType.Decimal);
                    cmd.Parameters.Add("@ProductId", SqlDbType.Int);

                    cmd.Parameters["@Name"].Value = product.Name;
                    cmd.Parameters["@Price"].Value = product.Price;
                    cmd.Parameters["@ProductId"].Value = product.ProductId;

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(dt);
                    }

                }

            }
        }

        public void DeleteProduct(int productId)
        {
            DataTable dt = new DataTable();

            using (SqlConnection con = new SqlConnection(connectionString))
            {

                con.Open();

                using (SqlCommand cmd = new SqlCommand("DELETE Product WHERE ProductID = @ProductID", con))
                {


                    cmd.Parameters.Add("@ProductId", SqlDbType.Int);

                    cmd.Parameters["@ProductId"].Value = productId;

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(dt);
                    }

                }

            }
        }
    }
}
