﻿using System.Collections.Generic;
using ProductManager.Entities;

namespace ProductManager.WebClient.Models
{
    public class PagingModel
    {
        public List<Product> Product { get; set; }
        public int CurrentPageIndex { get; set; }
        public int PageCount { get; set; }
    }
}
