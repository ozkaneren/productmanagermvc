﻿using System.Collections.Generic;
using ProductManager.Business.Abstraction;
using ProductManager.Entities;
using ProductManager.Repository.Abstraction;
using ProductManager.Repository.Implementation.Dapper;

namespace ProductManager.Business.Implementation
{
    public class ProductManager : IProductManager
    {
        private readonly IProductRepository _productRepository;
        private readonly IProductLogger _productLogger;
        private readonly IProductValidator _productValidator;

        public ProductManager()
        {
            _productRepository = new ProductRepository();
            _productLogger = new ProductFileLogger();
            _productValidator = new ProductValidator();
        }

        public List<Product> GetProducts()
        {
            List<Product> products = _productRepository.GetProducts();

            return products;
        }

        public Product GetProduct(int productId)
        {
            Product product = _productRepository.GetProduct(productId);

            _productLogger.LogProduct(product);

            return product;
        }

        public void AddProduct(Product product)
        {
            bool isValid = _productValidator.ValidateProduct(product);

            if (isValid)
            {
                _productRepository.CreateProduct(product);
            }
        }

        public void UpdateProduct(Product product)
        {
            bool isValid = _productValidator.ValidateProduct(product);

            Product existingProduct = _productRepository.GetProduct(product.ProductId);

            if (isValid && existingProduct != null)
            {
                _productRepository.UpdateProduct(product);
            }
        }

        public void DeleteProduct(int productId)
        {
            Product existingProduct = _productRepository.GetProduct(productId);

            if (existingProduct != null)
            {
                _productRepository.DeleteProduct(productId);
            }
        }
    }
}
